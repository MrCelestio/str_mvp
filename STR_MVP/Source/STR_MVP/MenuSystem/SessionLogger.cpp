// Fill out your copyright notice in the Description page of Project Settings.

#include "SessionLogger.h"
#include "Components/TextBlock.h"
#include "Components/Button.h"
#include "MainMenu.h"



void USessionLogger::Setup(class UMainMenu* InParent, uint32 InIndex)
{
	Parent = InParent;
	Index = InIndex;
	SessionIDButton->OnClicked.AddDynamic(this, &USessionLogger::OnClicked);
}

void USessionLogger::OnClicked()
{
	Parent->SelectIndex(Index);
}
