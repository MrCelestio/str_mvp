// Fill out your copyright notice in the Description page of Project Settings.

#include "OptionMenu.h"

#include "Components/Button.h"


bool UOptionMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	//TODO: setup
	if (!ensure(MenuButton != nullptr)) return false;
	MenuButton->OnClicked.AddDynamic(this, &UOptionMenu::ReturnToMainMenu);

	if (!ensure(CancelButton != nullptr)) return false;
	CancelButton->OnClicked.AddDynamic(this, &UOptionMenu::CloseMenu);

	
	return true;
}

void UOptionMenu::CloseMenu()
{
	this->RemoveFromViewport();
	if (!ensure(PlayerController != nullptr)) return;



	FInputModeGameOnly InputModeData;
	InputModeData.SetConsumeCaptureMouseDown(false);

	PlayerController->SetInputMode(InputModeData);

	PlayerController->bShowMouseCursor = false;
}

void UOptionMenu::SetMenuInterface(IMenuInterface* MenuInterface)
{
	this->MenuInterface = MenuInterface;
}

void UOptionMenu::OpenMenu()
{
	this->AddToViewport();

	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::LockAlways);

	PlayerController->SetInputMode(InputModeData);

	PlayerController->bShowMouseCursor = true;
}

void UOptionMenu::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	this->RemoveFromViewport();
	if (!ensure(PlayerController != nullptr)) return;



	FInputModeGameOnly InputModeData;
	InputModeData.SetConsumeCaptureMouseDown(false);

	PlayerController->SetInputMode(InputModeData);

	PlayerController->bShowMouseCursor = false;
}

void UOptionMenu::ReturnToMainMenu()
{
	if (MenuInterface != nullptr)
	{
		MenuInterface->ReturnToMainMenu();
	}
}
