// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"

#include "SessionLogger.generated.h"

/**
 * 
 */
UCLASS()
class STR_MVP_API USessionLogger : public UUserWidget
{
	GENERATED_BODY()

public:

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ServerName;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* HostUser;

	UPROPERTY(meta = (BindWidget))
	class UTextBlock* ConnectionFraction;

	UPROPERTY(BlueprintReadOnly)
	bool Selected;

	void Setup(class UMainMenu* Parent, uint32 Index);

	UFUNCTION()
	void OnClicked();

protected:



private:
	UPROPERTY(meta = (BindWidget))
	class UButton* SessionIDButton;

	UPROPERTY()
		class UMainMenu* Parent;

	uint32 Index;
};
