// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Blueprint/UserWidget.h"
#include "MenuInterface.h"
#include "OptionMenu.generated.h"

/**
 * 
 */
UCLASS()
class STR_MVP_API UOptionMenu : public UUserWidget
{
	GENERATED_BODY()

public:


	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;

	void SetMenuInterface(IMenuInterface* MenuInterface);
	
	UFUNCTION()
	void OpenMenu();
protected:

	virtual bool Initialize();


private:

	UPROPERTY(meta = (BindWidget))
	class UButton* MenuButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* CancelButton;



	UFUNCTION()
	void CloseMenu();



	UFUNCTION()
	void ReturnToMainMenu();


	IMenuInterface* MenuInterface;

	APlayerController* PlayerController;
};
