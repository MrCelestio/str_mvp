// Fill out your copyright notice in the Description page of Project Settings.

#include "MainMenu.h"

#include "UObject/ConstructorHelpers.h"
#include "MenuInterface.h"
#include "Blueprint/UserWidget.h"
#include "Components/Button.h"
#include "Components/WidgetSwitcher.h"
#include "Components/EditableTextBox.h"
#include "Components/TextBlock.h"

#include "SessionLogger.h"


UMainMenu::UMainMenu(const FObjectInitializer & ObjectInitializer):Super(ObjectInitializer)
{

	static ConstructorHelpers::FClassFinder<UUserWidget> SessionLoggerBPClass(TEXT("/Game/MenuSystem/WBP_SessionLogger"));
	if (!ensure(SessionLoggerBPClass.Class != nullptr)) return;
	SessionLoggerClass = SessionLoggerBPClass.Class;
}


bool UMainMenu::Initialize()
{
	bool Success = Super::Initialize();
	if (!Success) return false;

	//TODO: setup
	if (!ensure(HostButton != nullptr)) return false;
	HostButton->OnClicked.AddDynamic(this, &UMainMenu::HostServer);
	
	if (!ensure(JoinButton != nullptr)) return false;
	JoinButton->OnClicked.AddDynamic(this, &UMainMenu::OpenJoinMenu);

	if (!ensure(BackButton != nullptr)) return false;
	BackButton->OnClicked.AddDynamic(this, &UMainMenu::OpenMainMenu);

	if (!ensure(QuitButton != nullptr)) return false;
	QuitButton->OnClicked.AddDynamic(this, &UMainMenu::QuitGame);

	if (!ensure(JoinIPButton != nullptr)) return false;
	JoinIPButton->OnClicked.AddDynamic(this, &UMainMenu::JoinServer);

	return true;
}

void UMainMenu::SetMenuInterface(IMenuInterface* MenuInterface)
{
	this->MenuInterface = MenuInterface;
}

void UMainMenu::Setup()
{
	this->AddToViewport();
	
	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	PlayerController = World->GetFirstPlayerController();
	if (!ensure(PlayerController != nullptr)) return;

	FInputModeUIOnly InputModeData;
	InputModeData.SetWidgetToFocus(this->TakeWidget());
	InputModeData.SetLockMouseToViewportBehavior(EMouseLockMode::LockAlways);

	PlayerController->SetInputMode(InputModeData);

	PlayerController->bShowMouseCursor = true;

}
void UMainMenu::OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld)
{
	this->RemoveFromViewport();
	if (!ensure(PlayerController != nullptr)) return;



	FInputModeGameOnly InputModeData;
	InputModeData.SetConsumeCaptureMouseDown(false);

	PlayerController->SetInputMode(InputModeData);

	PlayerController->bShowMouseCursor = false;
}
void UMainMenu::HostServer()
{

	if (MenuInterface != nullptr)
	{
		MenuInterface->Host();
	}
}

void UMainMenu::SetServerList(TArray<FServerData> ServerNames)
{


	ServerList->ClearChildren();
	if (!ensure(SessionLoggerClass != nullptr)) return;
	int32 i = 0;
	for (const FServerData& ServerData : ServerNames)
	{
		USessionLogger* Log = CreateWidget<USessionLogger>(this, SessionLoggerClass);
		if (!ensure(Log != nullptr)) return;
		
		Log->ServerName->SetText(FText::FromString(ServerData.Name));
		Log->HostUser->SetText(FText::FromString(ServerData.HostUsername));
		FString FractionText = FString::Printf(TEXT("%d / %d"), ServerData.CurrentPlayers, ServerData.MaxPlayers);
		Log->ConnectionFraction->SetText(FText::FromString(FractionText));
		Log->Setup(this, i);
		++i;
		ServerList->AddChild(Log);
	}

	
	
}

void UMainMenu::SelectIndex(uint32 Index)
{
	SelectedIndex = Index;
	UpdateChildren();
}

void UMainMenu::UpdateChildren()
{
	for (int32 i = 0; i < ServerList->GetChildrenCount(); ++i)
	{
		auto Row = Cast<USessionLogger>(ServerList->GetChildAt(i));
		if (Row != nullptr)
		{
			Row->Selected = (SelectedIndex.IsSet() && SelectedIndex.GetValue() == i);
		}
	}
}
void UMainMenu::JoinServer()
{
	if (SelectedIndex.IsSet())
	{
		MenuInterface->Join(SelectedIndex.GetValue());
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Index not set!"))
	}
	if (MenuInterface != nullptr)
	{
		
	
	}
}

void UMainMenu::QuitGame()
{
	MenuInterface->QuitGame();
}

void UMainMenu::OpenJoinMenu()
{
	if (!ensure(MenuSwitcher != nullptr)) return;
	if (!ensure(JoinMenu != nullptr)) return;
	MenuSwitcher->SetActiveWidget(JoinMenu);
	if (MenuInterface != nullptr)
	{
		MenuInterface->RefreshSessionList();
	}
}

void UMainMenu::OpenMainMenu()
{
	if (!ensure(MenuSwitcher != nullptr)) return;
	if (!ensure(MainMenu != nullptr)) return;
	MenuSwitcher->SetActiveWidget(MainMenu);
}

