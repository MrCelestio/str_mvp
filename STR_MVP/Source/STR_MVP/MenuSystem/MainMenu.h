// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "MenuInterface.h"
#include "Blueprint/UserWidget.h"
#include "Widget.h"
#include "MainMenu.generated.h"

USTRUCT()
struct FServerData
{
	GENERATED_BODY()

	FString Name;
	uint16 CurrentPlayers;
	uint16 MaxPlayers;
	FString HostUsername;
};
/**
 * 
 */

UCLASS()

class STR_MVP_API UMainMenu : public UUserWidget
{
	GENERATED_BODY()

public:



	UMainMenu(const FObjectInitializer& ObjectInitializer);

	void SetMenuInterface(IMenuInterface* MenuInterface);

	void Setup();
	
	virtual void OnLevelRemovedFromWorld(ULevel* InLevel, UWorld* InWorld) override;

	void SetServerList(TArray<FServerData> ServerNames);

	void SelectIndex(uint32 Index);


	UPROPERTY(BlueprintReadWrite, meta = (BindWidget))
	class UWidgetSwitcher* MenuSwitcher;

protected:
	virtual bool Initialize() override;

private:

	TSubclassOf<class UUserWidget> SessionLoggerClass;

	UPROPERTY(meta = (BindWidget))
	class UButton* HostButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* JoinButton;

	UPROPERTY(meta = (BindWidget))
	class UButton* QuitButton;

	UPROPERTY(meta = (BindWidget))
		class UButton* JoinIPButton;

	UPROPERTY(meta = (BindWidget))
		class UButton* BackButton;
	
	UPROPERTY(meta = (BindWidget))
		class UPanelWidget* ServerList;




	UPROPERTY(meta = (BindWidget))
		class UWidget* JoinMenu;



	UPROPERTY(meta = (BindWidget))
		class UWidget* MainMenu;
	
	UFUNCTION()
		void HostServer();

	UFUNCTION()
	void JoinServer();

	UFUNCTION()
	void QuitGame();

	UFUNCTION()
		void OpenJoinMenu();

	UFUNCTION()
		void OpenMainMenu();

	IMenuInterface* MenuInterface;

	APlayerController* PlayerController;

	TOptional<uint32> SelectedIndex;

	void UpdateChildren();
};
