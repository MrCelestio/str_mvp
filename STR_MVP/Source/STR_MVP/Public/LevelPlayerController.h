// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "PlayerInterface.h"
#include "TurnBasedGameMode.h"
#include "LevelPlayerController.generated.h"

/**
 * 
 */
UCLASS()
class STR_MVP_API ALevelPlayerController : public APlayerController
{
	GENERATED_BODY()
	







public:
	virtual void BeginPlay();

	UFUNCTION(Exec)
	void TogglePlayerControlOn();

	UFUNCTION(Exec)
	void TogglePlayerControlOff();

	//bool DelegatePendingCombat();

	//bool DelegateTradeRequests();

	//bool CheckVictoryConditions();

	UFUNCTION(Exec)
		void EndTurn();

	UFUNCTION()
	void UpdatePhaseStatus(EPhase& InCurrentPhase);

UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	EPhase CurrentPhase;

UPROPERTY(BlueprintReadWrite)
	class ATurnBasedGameMode* MainGameMode;

private:

	

	
	//List of combat orders


	//List of trade requests


	//Victory point count
	

};
