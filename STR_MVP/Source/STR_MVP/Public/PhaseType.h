// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define GETENUMSTRING(etype, evalue) ( (FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true) != nullptr) ? FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true)->GetEnumName((int32)evalue) : FString("Invalid - are you sure enum uses UENUM() macro?") )

#include "CoreMinimal.h"
#include "Engine/UserDefinedEnum.h"
#include "PhaseType.generated.h"

/**
 * 
 */


UENUM(BlueprintType)		//"BlueprintType" is essential to include
		enum class EPhase : uint8
	{
		EP_Player 	UMETA(DisplayName = "Player"),
		EP_Movement 	UMETA(DisplayName = "Movemnt"),
		EP_Combat 	UMETA(DisplayName = "Comabat"),
		EP_Trade 	UMETA(DisplayName = "Trade"),
		EP_End 	UMETA(DisplayName = "End")
	};
UCLASS()
class STR_MVP_API UPhaseType : public UUserDefinedEnum
{
	GENERATED_BODY()


		
	
};
