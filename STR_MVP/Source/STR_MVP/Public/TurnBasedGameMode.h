// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#define GETENUMSTRING(etype, evalue) ( (FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true) != nullptr) ? FindObject<UEnum>(ANY_PACKAGE, TEXT(etype), true)->GetEnumName((int32)evalue) : FString("Invalid - are you sure enum uses UENUM() macro?") )

#include "CoreMinimal.h"
#include "Engine/World.h"
#include "UObject/Class.h"
#include "GameFramework/GameModeBase.h"
#include "PhaseType.h"
#include "LevelPlayerController.h"
#include "TurnBasedGameMode.generated.h"



DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FPhaseChangeEventDelegate_OnPhaseChange, EPhase, CurrentPhase);
/**
 * 
 */



UCLASS()	
class STR_MVP_API ATurnBasedGameMode : public AGameModeBase
{
	GENERATED_BODY()


public:

	void Tick(float DeltaTime) override;

	void BeginPlay() override;


	//Store current phase
	UPROPERTY(BlueprintReadWrite, VisibleAnywhere)
	EPhase currentPhase;


	//Add player controller to PlayerConnectedList
	


	//Add player controller to PlayerReadyList
	UFUNCTION(BlueprintCallable)
	bool AddPlayerToReadyList(ALevelPlayerController* PlayerController);


	//Amount of player in current session
	UPROPERTY(BlueprintReadWrite)
	int32 PlayerCount = 1;

	//List of all players in current session
		UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = PlayerLists)
		TArray<APlayerController*> PlayerConnectedList{};

	//List of ready players
		UPROPERTY(BlueprintReadWrite, VisibleAnywhere, Category = PlayerLists)
		TArray<APlayerController*> PlayerReadyList{};

	//Delegate current phase


	//Check phase end

	UFUNCTION()
	bool isPhaseReady(EPhase Phase);

	//Change to next phase

	UFUNCTION(BlueprintCallable)
	bool changeToNextPhase();


	UPROPERTY(BlueprintAssignable, BlueprintCallable, VisibleAnywhere)
	FPhaseChangeEventDelegate_OnPhaseChange OnPhaseChange;
protected:




private:

	








	//Delegate move order to ships

	//Check victory conditions of players


	//End match with a winner
	
};