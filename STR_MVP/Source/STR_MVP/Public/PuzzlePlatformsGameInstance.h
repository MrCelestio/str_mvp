// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "MenuSystem/MenuInterface.h"
#include "MenuSystem/MainMenu.h"
#include "MenuSystem/OptionMenu.h"
#include "OnlineSubsystem.h"
#include "OnlineSessionInterface.h"
#include "PuzzlePlatformsGameInstance.generated.h"


/**
 * 
 */
UCLASS() 
class STR_MVP_API UPuzzlePlatformsGameInstance : public UGameInstance , public IMenuInterface
{
	GENERATED_BODY()
	

public:

	UPuzzlePlatformsGameInstance(const FObjectInitializer& ObjectInitializer);

	virtual void Init();

	UFUNCTION(Exec, BlueprintCallable)
		void LoadMenu();

	UFUNCTION(Exec, BlueprintCallable)
		void LoadOptionMenu();

	UFUNCTION(Exec)
		void Host();

	UFUNCTION(Exec)
		void QuitGame();

	UFUNCTION(Exec)
		virtual void RefreshSessionList();

	UFUNCTION(Exec)
		void Join(int32 Index);

	void StartSession();

	UFUNCTION(Exec)
		virtual void ReturnToMainMenu();



private:

	TSubclassOf<class UUserWidget> MenuClass;
	TSubclassOf<class UUserWidget> OptionMenuClass;

	TSharedPtr<class FOnlineSessionSearch> SessionSearch;

	class UMainMenu* Menu;

	class UOptionMenu* OptionMenu;

	IOnlineSessionPtr SessionInterface;

	void OnCreateSessionComplete(FName SessionName, bool Success);

	void CreateSession();

	void OnDestroySessionComplete(FName SessionName, bool Success);

	void OnFindSessionComplete(bool Success);

	void OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result);


};
