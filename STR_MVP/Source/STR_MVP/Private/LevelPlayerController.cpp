// Fill out your copyright notice in the Description page of Project Settings.

#include "LevelPlayerController.h"




void ALevelPlayerController::BeginPlay()
{
	Super::BeginPlay();

	MainGameMode = (ATurnBasedGameMode*)GetWorld()->GetAuthGameMode();
	if (!ensure(MainGameMode != nullptr)) return;

	UpdatePhaseStatus(MainGameMode->currentPhase);
}

void ALevelPlayerController::TogglePlayerControlOn()
{
	FInputModeGameAndUI DualInput;
	DualInput.SetLockMouseToViewportBehavior(EMouseLockMode::LockAlways);


	SetInputMode(DualInput);
}

void ALevelPlayerController::TogglePlayerControlOff()
{
	FInputModeUIOnly HudInput;
	HudInput.SetLockMouseToViewportBehavior(EMouseLockMode::DoNotLock);


	SetInputMode(HudInput);
}

void ALevelPlayerController::EndTurn()
{
	MainGameMode->AddPlayerToReadyList(this);
}

void ALevelPlayerController::UpdatePhaseStatus(EPhase& InCurrentPhase)
{
	this->CurrentPhase = InCurrentPhase;
}
