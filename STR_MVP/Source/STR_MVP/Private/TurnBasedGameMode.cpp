// Fill out your copyright notice in the Description page of Project Settings.

#include "TurnBasedGameMode.h"


#include "UObject/Class.h"


void ATurnBasedGameMode::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);



}


void ATurnBasedGameMode::BeginPlay() {


	Super::BeginPlay();
	
	currentPhase = EPhase::EP_Player;
	
}

bool ATurnBasedGameMode::AddPlayerToReadyList(ALevelPlayerController* PlayerController)
{
	if (PlayerController != nullptr)
	{
		PlayerReadyList.AddUnique(PlayerController);

		UE_LOG(LogTemp, Warning, TEXT("Adding Player to ready list: %s"), *PlayerController->GetName());
		
		isPhaseReady(currentPhase);
		return true;
	}
	return false;
}


bool ATurnBasedGameMode::isPhaseReady(EPhase Phase)
{
	//If available players is the same amount as the players that are ready AND game session is in progress
			//return ready

	if (PlayerCount == PlayerReadyList.Num())
	{
		UE_LOG(LogTemp, Warning, TEXT("Proccessing Phase change. Current phase: %s"), *GETENUMSTRING("EPhase", currentPhase));

		PlayerReadyList.Empty();

		changeToNextPhase();

		//TODO broadcast signal for phase change
		return true;
	}
	UE_LOG(LogTemp, Warning, TEXT("Player List: %d"), PlayerReadyList.Num());

	

	return false;
}

bool ATurnBasedGameMode::changeToNextPhase()
{
	
	switch (currentPhase)
	{
	case EPhase::EP_Player:

		

		currentPhase = EPhase::EP_Movement;
		//Move all ships pending movement

		OnPhaseChange.Broadcast(currentPhase);

		UE_LOG(LogTemp, Warning, TEXT("Current phase: %s"), *GETENUMSTRING("EPhase", currentPhase));

		return true;
	case EPhase::EP_Movement:
		
		currentPhase = EPhase::EP_Combat;
		//test for combat situations

		OnPhaseChange.Broadcast(currentPhase);
		UE_LOG(LogTemp, Warning, TEXT("Current phase: %s"), *GETENUMSTRING("EPhase", currentPhase));

		return true;
	case EPhase::EP_Combat:

		

		currentPhase = EPhase::EP_Trade;
		//check for pending trades between players

		OnPhaseChange.Broadcast(currentPhase);
		UE_LOG(LogTemp, Warning, TEXT("Current phase: %s"), *GETENUMSTRING("EPhase", currentPhase));

		return true;
	case EPhase::EP_Trade:


		currentPhase = EPhase::EP_End;

		OnPhaseChange.Broadcast(currentPhase);
		UE_LOG(LogTemp, Warning, TEXT("Current phase: %s"), *GETENUMSTRING("EPhase", currentPhase));

		return true;
	case EPhase::EP_End:
		//TODO check for player victory


		currentPhase = EPhase::EP_Player;

		OnPhaseChange.Broadcast(currentPhase);
		UE_LOG(LogTemp, Warning, TEXT("Current phase: %s"), *GETENUMSTRING("EPhase", currentPhase));

		return true;
	
	}

	return false;
}



