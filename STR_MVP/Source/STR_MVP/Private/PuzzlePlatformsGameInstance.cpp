// Fill out your copyright notice in the Description page of Project Settings.

#include "PuzzlePlatformsGameInstance.h"


#include "GameFramework/PlayerController.h"


#include "UObject/ConstructorHelpers.h"
#include "Blueprint/UserWidget.h"
#include "OnlineSessionSettings.h"
#include "Engine/Engine.h"


const static FName SESSION_NAME = TEXT("Game");


UPuzzlePlatformsGameInstance::UPuzzlePlatformsGameInstance(const FObjectInitializer& ObjectInitializer)
{


static ConstructorHelpers::FClassFinder<UUserWidget> MainMenuBPClass(TEXT("/Game/MenuSystem/WBP_MainMenu"));
if (!ensure(MainMenuBPClass.Class != nullptr)) return;
MenuClass = MainMenuBPClass.Class;

static ConstructorHelpers::FClassFinder<UUserWidget> OptionMenuBPClass(TEXT("/Game/MenuSystem/WBP_OptionsMenu"));
if (!ensure(OptionMenuBPClass.Class != nullptr)) return;
OptionMenuClass = OptionMenuBPClass.Class;

UE_LOG(LogTemp, Warning, TEXT("Found class %s and %s"), *MainMenuBPClass.Class->GetName(), *OptionMenuBPClass.Class->GetName());
}

void UPuzzlePlatformsGameInstance::Init()
{
	IOnlineSubsystem* Subsystem = IOnlineSubsystem::Get();
	if (Subsystem != nullptr)
	{
		UE_LOG(LogTemp, Warning, TEXT("Found Subsystem %s"), *Subsystem->GetSubsystemName().ToString());
		SessionInterface = Subsystem->GetSessionInterface();

		if (SessionInterface.IsValid())
		{

			SessionInterface->OnCreateSessionCompleteDelegates.AddUObject(this, &UPuzzlePlatformsGameInstance::OnCreateSessionComplete);
			SessionInterface->OnDestroySessionCompleteDelegates.AddUObject(this, &UPuzzlePlatformsGameInstance::OnDestroySessionComplete);
			SessionInterface->OnFindSessionsCompleteDelegates.AddUObject(this, &UPuzzlePlatformsGameInstance::OnFindSessionComplete);
			SessionInterface->OnJoinSessionCompleteDelegates.AddUObject(this, &UPuzzlePlatformsGameInstance::OnJoinSessionComplete);
			SessionSearch = MakeShareable(new FOnlineSessionSearch());

		}
	}
	else
	{
		UE_LOG(LogTemp, Warning, TEXT("Found no Subsystem"));
	}
}



void UPuzzlePlatformsGameInstance::LoadMenu()
{
	if (!ensure(MenuClass != nullptr)) return;
	Menu = CreateWidget<UMainMenu>(this, MenuClass);
	if (!ensure(Menu != nullptr)) return;

	Menu->Setup();
	Menu->SetMenuInterface(this);
}

void UPuzzlePlatformsGameInstance::LoadOptionMenu()
{
	if (!ensure(OptionMenuClass != nullptr)) return;
	OptionMenu = CreateWidget<UOptionMenu>(this, OptionMenuClass);
	if (!ensure(OptionMenu != nullptr)) return;

	OptionMenu->OpenMenu();
	OptionMenu->SetMenuInterface(this);
}

void UPuzzlePlatformsGameInstance::Host()
{
	if (SessionInterface.IsValid())
	{
		auto ExistingSession = SessionInterface->GetNamedSession(SESSION_NAME);
		if (ExistingSession != nullptr)
		{
			SessionInterface->DestroySession(SESSION_NAME);
		}
		else
		{
			CreateSession();
		}

	}

}

void UPuzzlePlatformsGameInstance::OnCreateSessionComplete(FName SessionName, bool Success)
{
	if (!Success)
	{
		UE_LOG(LogTemp, Warning, TEXT("Cannot create session. A session already exists."));
		return;
	}
	UEngine* Engine = GetEngine();
	if (!ensure(Engine != nullptr)) return;

	Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, TEXT("Hosting"));

	UWorld* World = GetWorld();
	if (!ensure(World != nullptr)) return;

	World->ServerTravel("/Game/Levels/Ground_Zero_Level?listen");

}
void UPuzzlePlatformsGameInstance::CreateSession()
{
	if (SessionInterface.IsValid())
	{
		FOnlineSessionSettings SessionSettings;
		if(IOnlineSubsystem::Get()->GetSubsystemName() == "NULL")
		{
			SessionSettings.bIsLANMatch = true;
		}
		else
		{
			SessionSettings.bIsLANMatch = false;
		}

		SessionSettings.NumPublicConnections = 4;
		SessionSettings.bShouldAdvertise = true;
		SessionSettings.bUsesPresence = true;
		SessionSettings.Set(TEXT("Test"), FString("Hello"), EOnlineDataAdvertisementType::ViaOnlineServiceAndPing);

			SessionInterface->CreateSession(0, SESSION_NAME, SessionSettings);
	}
	
}

void UPuzzlePlatformsGameInstance::OnDestroySessionComplete(FName SessionName, bool Success)
{
	if (Success)
	{
		CreateSession();
	}
}

void UPuzzlePlatformsGameInstance::OnFindSessionComplete(bool Success)
{
	UE_LOG(LogTemp, Warning, TEXT("Finished searching for sessions!!!"));
	if (Success && SessionSearch.IsValid())
	{
		
		TArray<FServerData> ServerNames;

		for (const FOnlineSessionSearchResult& key : SessionSearch->SearchResults)
		{
			UE_LOG(LogTemp, Warning, TEXT("Session ID: %s"), *key.GetSessionIdStr());
			FServerData Data;
			Data.Name = key.GetSessionIdStr();
			Data.MaxPlayers = key.Session.SessionSettings.NumPublicConnections;
			Data.CurrentPlayers =Data.MaxPlayers - key.Session.NumOpenPublicConnections;
			Data.HostUsername = key.Session.OwningUserName;
			FString TestString;
			if (key.Session.SessionSettings.Get(TEXT("Test"), TestString))
			{
				UE_LOG(LogTemp, Warning, TEXT("Data found in settings: %s"), *TestString);
			}
			ServerNames.Add(Data);
		}
		Menu->SetServerList(ServerNames);
	}
	
}

void UPuzzlePlatformsGameInstance::OnJoinSessionComplete(FName SessionName, EOnJoinSessionCompleteResult::Type Result)
{

	if (!SessionInterface.IsValid())return;
	
		FString ConnectInfo;
		if (!SessionInterface->GetResolvedConnectString(SessionName, ConnectInfo))
		{
			UE_LOG(LogTemp, Warning, TEXT("Could not connect to server"))
		}

		if (!ConnectInfo.IsEmpty())
		{
			UEngine* Engine = GetEngine();
			if (!ensure(Engine != nullptr)) return;

			Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, FString::Printf(TEXT("Joining : %s"), *ConnectInfo));

			APlayerController* PlayerController = GetFirstLocalPlayerController();
			if (!ensure(PlayerController != nullptr)) return;
			PlayerController->ClientTravel(ConnectInfo,ETravelType::TRAVEL_Absolute);
		}
	
	
}

void UPuzzlePlatformsGameInstance::QuitGame()
{
	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;
	PlayerController->ConsoleCommand("quit", true);

}

void UPuzzlePlatformsGameInstance::Join( int32 Index)
{
	if (!SessionInterface.IsValid()) return;
	if (!SessionSearch.IsValid()) return;




	SessionInterface->JoinSession(0, SESSION_NAME, SessionSearch->SearchResults[Index]);
	



}

void UPuzzlePlatformsGameInstance::RefreshSessionList()
{
	SessionSearch->MaxSearchResults = 100;
	SessionSearch->QuerySettings.Set(SEARCH_PRESENCE, true, EOnlineComparisonOp::Equals);
	SessionInterface->FindSessions(0, SessionSearch.ToSharedRef());
}

void UPuzzlePlatformsGameInstance::StartSession()
{
	if (SessionInterface.IsValid())
	{
		SessionInterface->StartSession(SESSION_NAME);
	}
}

void UPuzzlePlatformsGameInstance::ReturnToMainMenu()
{
	UEngine* Engine = GetEngine();
	if (!ensure(Engine != nullptr)) return;

	Engine->AddOnScreenDebugMessage(0, 2, FColor::Green, FString::Printf(TEXT("Returning to Menu...")));

	APlayerController* PlayerController = GetFirstLocalPlayerController();
	if (!ensure(PlayerController != nullptr)) return;
	PlayerController->ClientTravel("/Game/MenuSystem/MainMenu",ETravelType::TRAVEL_Absolute);
}
